using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertV1 : Alert
{
    private void OnMouseDown () {
        Log.AddLog(1, drone.getNbDronesActifs(), getAlertStatus(), System.DateTimeOffset.UtcNow.ToUnixTimeSeconds() - timeInit, length);

        SetInitialState();
    }
}
