using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerManager : MonoBehaviour
{
    public GameObject marker2;
    public GameObject marker3;
    public GameObject marker4;
    public GameObject marker5;

    public GameObject[] marker_tab;

    public GameObject validation;

    protected GameObject marker;

    private static int nbMarker = 0;

    void Start() {
        marker_tab = new GameObject[] {marker2, marker3, marker4, marker5};
    }

    public void MarkerCorner() {
        marker = Instantiate(marker_tab[nbMarker], gameObject.transform);

        if (nbMarker == 3) {
            Destroy(GameObject.Find("MarkerButton"));
            Instantiate(validation, gameObject.transform);

            nbMarker = 0;
        } else {
            nbMarker++;
        }
    }
}
