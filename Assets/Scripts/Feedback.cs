using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Feedback : MonoBehaviour
{
    public bool isClone = false;
    public bool isOut = false;
    public GameObject feedback;

    Color color;

    void Update() {
        if (isClone) {
            // Fade apparition disparition
            if(!isOut) {
                if(color.a < 0.2f) {
                    color = GetComponent<Text>().color;

                    Color newCol = new Color(color.r, color.g, color.b, color.a + 0.005f);
                    GetComponent<Text>().color = newCol;
                } else {
                    StartCoroutine(waitForDestroy());  
                }
            } else {
                if(color.a > 0) {
                    color  = GetComponent<Text>().color;

                    Color newCol = new Color(color.r, color.g, color.b, color.a - 0.005f);
                    GetComponent<Text>().color = newCol;
                } else {
                    Destroy(gameObject);
                }
            }
        }
    }

    IEnumerator waitForDestroy () {    
        yield return new WaitForSeconds(0.5f);
       
        isOut = true;
    }

    public void launchFeedback(string value, GameObject gameObjectPosition = null) {

        Vector3 position;
        GameObject table = GameObject.Find("CanvaTable")/*.transform.GetChild(5).gameObject*/;
        int size = 3;

        if (gameObjectPosition == null) {
            gameObjectPosition = table;
            size = 5;
        }
            
        position = new Vector3(gameObjectPosition.transform.position.x, gameObjectPosition.transform.position.y, gameObjectPosition.transform.position.z);
        
      
        feedback = Instantiate(gameObject, position, table.transform.rotation, GameObject.Find("CanvaTable").transform/*.GetChild(5).gameObject.transform*/);  
       
        feedback.GetComponent<Feedback>().isClone = true;
        feedback.GetComponent<Transform>().localScale = new Vector3(size, size, 1);
        feedback.GetComponent<Text>().text = value;
        feedback.GetComponent<Text>().color = (value.Substring(0, 1) == "-") ? new Color(0.7f, 0.04f, 0.04f, 0f) : new Color(0.04f, 0.7f, 0.04f, 0f);
    }
}
