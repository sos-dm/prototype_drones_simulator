using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitializeEnv : MonoBehaviour
{
    public GameObject droneModel;
    public GameObject alertModel;

    public int TotalNumberOfDrones = 0;
    public int TotalNumberOfAlarme = 3;

    private GameObject[] allDrones;
    private GameObject[] allAlert;
    

    // Start is called before the first frame update
    void Start()
    {
        createDrones(TotalNumberOfDrones, droneModel); 
        createAlert(TotalNumberOfAlarme, alertModel);

        Points.init();

        GameObject.Find("Points").GetComponent<Text>().text = "Points " + Points.getPoints().ToString();
    }


    private void createDrones(int numberOfDroneToCreate, GameObject droneModel)
    {
        GameObject aDrone;
        Transform origine;

        int i;
        float x, y, z;
        Vector3 dronePositionOnTheScene;

        origine = droneModel.transform;
        
        for (i = 0; i < numberOfDroneToCreate; i++)
        {
            x = Random.Range(-6f, 6f);
            y = droneModel.transform.position.y;
            z = Random.Range(-3f, 3f);

            dronePositionOnTheScene = new Vector3(x, y, z);
            aDrone = Instantiate(droneModel, dronePositionOnTheScene, origine.rotation);
            aDrone.transform.localScale = new Vector3(0.7f, 1f, 0.7f);
        }
    }

    private void createAlert(int numberOfAlertToCreate, GameObject alertModel)
    {
        GameObject aAlert;
        Transform origine;

        int i;
        float x, y, z;
        Vector3 alertPositionOnTheScene;

        origine = alertModel.transform;
        
        for (i = 0; i < numberOfAlertToCreate - 1; i++)
        {
            x = (i < 2) ? origine.position.x * -1 : origine.position.x;
            y = origine.position.y;
            z = (i > 0) ? origine.position.z * -1 : origine.position.z;

            alertPositionOnTheScene = new Vector3(x, y, z);
            aAlert = Instantiate(alertModel, alertPositionOnTheScene, origine.rotation);
            aAlert.GetComponent<Alert>().setId(i + 2);
        }

        aAlert = Instantiate(alertModel, origine.position, origine.rotation);
        aAlert.GetComponent<Alert>().setId(1);
    }
}

