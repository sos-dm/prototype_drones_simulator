using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class ChangeScene : MonoBehaviour {

    public GameObject tableModel;
    private static bool isTuto = false;

    void Update () {
        if(TCPServer2.init) {
            initSimulation();
            TCPServer2.init = false;
        }
    }

    public void LoadScene(string SceneToLoad)
    {
        SceneManager.LoadScene(SceneToLoad);
    }

    public void setFlash(bool flash) {
        Alert.setFlash(flash);
    }

    public void setSound(bool sound) {
        Alert.setSound(sound);
    }

    public void setIsTuto(bool tuto) {
        isTuto = tuto;
    }

    private void initSimulation() {
        if (Log.isServer) {
            Destroy(GameObject.Find("CanvaMarker(Clone)"));
        }
        
        if (isTuto) {
            tableModel.GetComponent<InitializeEnv>().TotalNumberOfDrones = 5;
        }

        Instantiate(tableModel);
    }
}