using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderEvents : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private bool sliderSelect = false;

    public void OnPointerUp(PointerEventData eventData)
    {
        sliderSelect = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        sliderSelect = true;
    }

    public bool getSliderSelect() {
        return sliderSelect;
    }
}