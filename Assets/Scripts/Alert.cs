using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using static Log;

public class Alert : MonoBehaviour
{
    protected enum Status { free, notFree };
    protected static Status status;
    protected bool isAlert = false;
    private bool inTraitement = false;
    protected long timeInit;
    public int length = 5; // Duration of the alert in second

    public int proba = 20;

    protected DroneAI drone = new DroneAI();

    protected GameObject flash;

    public static bool isFlash;
    public static bool isSound;

    private int id = 0;


    // Start is called before the first frame update
    protected void Start()
    {
        status = Status.free;
    }

    // Update is called once per frame
    void Update()
    {
        // Probabilité alerte 1 fois par seconde
        if (status == Status.free && !inTraitement) {
            inTraitement = true;
            TriggerAlert();
        }
    }


    private void TriggerAlert() {
        StartCoroutine(waitForRandomAlert());
    }

    IEnumerator waitForRandomAlert () {    
        if(RandomProbaAlert()) {
            if(status == Status.notFree) {
                yield return new WaitForSeconds(1f);
                yield break;
            }
            
            // Allume
            status = Status.notFree;

            GetComponentInChildren<Light>().intensity = 2;
            GetComponentInChildren<Renderer>().material.color = new Color32(241, 171, 171, 255);

            // Network
            if(Log.isServer) {
                GameObject.Find("SceneControl").GetComponent<TCPServer2>().sendMessageToClient("startAlert," + id);
            }

            // Flash
            if(isFlash) {
                flash = Instantiate(GameObject.Find("Flash"));  
                flash.GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().position.x, flash.GetComponent<Transform>().position.y, GetComponent<Transform>().position.z);
                flash.GetComponent<Light>().intensity = 3f;
            }   

            // Son
            if(isSound) {
                GameObject.Find("Sound").GetComponent<AudioSource>().Play();
            }
            
            isAlert = true;
            timeInit = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            StartAlert();
        }

        yield return new WaitForSeconds(1f);

        inTraitement = false;
    }  


    private void StartAlert() {
        StartCoroutine(waitForEndAlert());
    }

    IEnumerator waitForEndAlert () {    
        yield return new WaitForSeconds(length);
    
        if (isAlert) {
            Points.setPoints(Points.getPoints() - 5);
            GameObject.Find("Points").GetComponent<Text>().text = "Points " + Points.getPoints().ToString();

            Log.AddLog(3, drone.getNbDronesActifs(), getAlertStatus(), length, length);

            GameObject.Find("Feedback").GetComponent<Feedback>().launchFeedback("-5");
            SetInitialState();
        }
    }

    protected void SetInitialState() {
        GetComponentInChildren<Light>().intensity = 0;
        GetComponentInChildren<Renderer>().material.color = new Color32(200, 0, 0, 255);

        StopCoroutine(waitForEndAlert());

        status = Status.free;
        isAlert = false;

        if(isFlash) {
            Destroy(flash);
        }
    }


    protected bool RandomProbaAlert() {
        var rand = Random.Range(0, proba + 1);
        
        return (rand <= proba - 1) ? false : true;
    }

    public bool getAlertStatus() {
        return (status == Status.free) ? false : true;
    }

    public static void setFlash(bool flash) {
        isFlash = flash;
    }

    public static void setSound(bool sound) {
        isSound = sound;
    }

    public void setId(int i) {
        id = i;
    }
}
