using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Points
{
    private static int points;

    public static void init() {
        points = 0;
    }

    public static int getPoints() {
        return points;
    }

    public static void setPoints(int p) {
        points = p;
    }
}
