using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

using UnityEngine;


public class TCPServer2 : MonoBehaviour
{    
    public string PortNumber = "8080";

    private static bool isServer = false;

    private System.Net.Sockets.TcpListener socket;
    private System.Net.Sockets.TcpClient client;

    public GameObject tableModel;
    public GameObject markerModel;
    private GameObject marker;

    public static bool init = false;

    void Start()
    {
        Debug.Log("Start here");
        StartServer();
    }

    private void StartServer()
    {
        if(isServer) {
            //Instantiate(tableModel);
            try {
                /*IPEndPoint ip = new IPEndPoint(IPAddress.Any, 8080);
                socket = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);

                socket.Bind(ip);
                socket.Listen(10);
                Debug.Log("Waiting for a client...");
                client = socket.Accept();
                IPEndPoint clientep =(IPEndPoint)client.RemoteEndPoint;
                Debug.Log("Connected with "+ clientep.Address+" at port " + clientep.Port);*/

                // Unable to connect to server.System.Net.Sockets.SocketException (0x80004005): Aucune connexion n’a pu être établie car l’ordinateur cible l’a expressément refusée.
                socket = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, int.Parse(PortNumber));
                socket.Start();

                Debug.Log("server is listening at " + System.Net.IPAddress.Any + ":" + int.Parse(PortNumber) + "...");

                marker = Instantiate(markerModel);

                // The ConnectionReceived event is raised when connections are received.
                socket.BeginAcceptTcpClient(new AsyncCallback(AcceptTcpClientCallback), socket);
            } 
            catch (SocketException e)
            {
                Debug.Log("Unable to connect to client. " + e.ToString());
                return;
            }
        } else {
            //Instantiate(tableModel);
            init = true;
        }
    }

    private void AcceptTcpClientCallback(IAsyncResult ar)
    {
        // Display the received data on the console.
        client = socket.EndAcceptTcpClient(ar);

        IPEndPoint clientep =(IPEndPoint)client.Client.RemoteEndPoint;
        Debug.Log("Connected with "+ clientep.Address+" at port " + clientep.Port);

        init = true;
    }

    public static void setIsServer(bool s) {      
        isServer = s;
    }

    public void sendMessageToClient(string msg) {
        
        byte[] data = new byte[1024];
        data = Encoding.ASCII.GetBytes(msg);
        client.Client.Send(data, data.Length,SocketFlags.None);

        Debug.Log("send msg : " + msg);
    }

    public void closeConnexion() {
        IPEndPoint clientep =(IPEndPoint)client.Client.RemoteEndPoint;
        
        Debug.Log("Disconnected from " + clientep.Address);
        client.Close();
        socket.Stop();
    }
}
