using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DroneAISc1_V1 : DroneAI
{
    private GameObject sliderModel;
    private GameObject sliderClone;
    private Slider mainSlider;
    private bool isSlider = false;
    private int sliderStart;
    private int sliderStop;

    new void Start()
    {
        base.Start();
        sliderModel = GameObject.Find("CanvasSlider");
    }

    new void Update()
    {
        base.Update();

        // Verif de la valeur du slider si isSlider et EndDrag
        if(isSlider && !mainSlider.GetComponent<SliderEvents>().getSliderSelect()) {
            ValueChangeCheck();
        }
    }


    // Action click sur le drone
    private void OnMouseDown () {
        if(!EventSystem.current.IsPointerOverGameObject() && !isSlider && status == Status.OnEvent) {
            createSlider(sliderModel);
            isSlider = true;
        }
    }


    private void createSlider(GameObject slider) {
        float x, y, z;
        Transform drone;
        Vector3 sliderPositionOnTheScene;
        Vector3 sliderStopPosition;

        // Position slider
        drone = gameObject.transform;

        x = drone.position.x;
        y = slider.transform.position.y;
        z = drone.position.z;

        sliderPositionOnTheScene = new Vector3(x, y, z);

        // Valeur initiale et finale slider
        sliderStart = Random.Range(0, 11);
        sliderStop = sliderStart;
        
        while(sliderStop == sliderStart) {
            sliderStop = Random.Range(0, 11);
        }

        
        // Instanciation et configuration slider clone à partir du modele
        sliderClone = Instantiate(slider, sliderPositionOnTheScene, slider.transform.rotation, gameObject.transform);

        mainSlider = sliderClone.transform.GetChild(0).gameObject.GetComponentInChildren<Slider>();
        
        sliderStopPosition = mainSlider.transform.GetChild(2).transform.GetChild(1).gameObject.GetComponent<RectTransform>().transform.localPosition;
        mainSlider.transform.GetChild(2).transform.GetChild(1).gameObject.GetComponent<RectTransform>().transform.localPosition = new Vector3(sliderStopPosition.x + 14 * sliderStop, sliderStopPosition.y, sliderStopPosition.z);
        
        mainSlider.value = sliderStart;
    }


    public void ValueChangeCheck()
    {   
        if(mainSlider.value == sliderStop) {
            // Incrémentation de 1 point
            Points.setPoints(Points.getPoints() + 1);
            GameObject.Find("Points").GetComponent<Text>().text = "Points " + Points.getPoints().ToString();

            // feedback
            GameObject.Find("Feedback").GetComponent<Feedback>().launchFeedback("+1", gameObject);

            isSlider = false;

            // Changement de couleur slider validation
            sliderClone.GetComponentInChildren<Image>().color = new Color32(0, 200, 100, 215);

            // Log   
            Log.AddLog(0, nbDronesActifs, alert.getAlertStatus(), System.DateTimeOffset.UtcNow.ToUnixTimeSeconds() - eventTimeInit, eventDuration);
            nbDronesActifs--;

            // 0.5 seconde pour voir la couleur
            StartCoroutine(waitForCloseSlider());
        }
    }


    IEnumerator waitForCloseSlider () {    
        mainSlider.interactable = false;

        yield return new WaitForSeconds(0.5f);
    
        SetInitialState();

        Destroy(sliderClone);
    }


    protected override void FinishEvent() {
        
        nbDronesActifs--;

        //if(Feedback.isFeedback) {
            GameObject.Find("Feedback").GetComponent<Feedback>().launchFeedback("-1",  gameObject);
        //}

        if (isSlider) {
            isSlider = false;

            // Changement de couleur slider echec
            sliderClone.GetComponentInChildren<Image>().color = new Color32(200, 0, 0, 215);

            // 0.5 seconde pour voir la couleur
            StartCoroutine(waitForCloseSlider());

        } else {
            SetInitialState();
        }
    }
}
