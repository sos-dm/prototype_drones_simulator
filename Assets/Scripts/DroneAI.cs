using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.UI;

using static Log;

public class DroneAI : MonoBehaviour
{
    protected float speed = 2f;

    protected Vector3 _destination;
    protected Vector3 _direction;

    protected float _rayDistance = 1f;
    [SerializeField] protected LayerMask _layerMask;

    protected enum Status { NoTreatment, OnEvent, Failed };
    protected Status status;
    protected bool onTreatment = false;

    public static int nbDronesActifs = 0;
    protected static Alert alert = new Alert();

    protected long eventTimeInit;
    
    public int eventDuration = 15;
    public int proba = 20;

 
    protected void Start()
    {
        status = Status.NoTreatment;
        nbDronesActifs = 0;
    }

    // Update is called once per frame
    protected void Update()
    {
        // Gestion déplacement + event
        if(status != Status.OnEvent) {
            if(IsPathBlocked()) {
                SetDestination();
            } 
            if(!onTreatment) {
                TriggerEvent();
            }
        }
    }


    protected void SetInitialState() {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        
        GetComponent<Renderer>().material.color = new Color32(15, 243, 221, 255);
        GetComponent<Rigidbody>().velocity = _direction * speed;

        status = Status.NoTreatment;
    }


    // Détection collision
    protected bool IsPathBlocked()
    {
        UnityEngine.Ray ray = new Ray(transform.position, _direction);
        RaycastHit hit;
        return Physics.Raycast(ray, out hit, _rayDistance, _layerMask);
    }

    void OnCollisionEnter() {
        if(status == Status.NoTreatment) {
            SetDestination();
        }
    }

    // Nouvelle destination aléatoire
    protected void SetDestination()
    {
        _destination = new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 0f, UnityEngine.Random.Range(-4.5f, 4.5f));

        _direction = Vector3.Normalize(_destination - transform.position);
        _direction = new Vector3(_direction.x, 0f, _direction.z);

        GetComponent<Rigidbody>().velocity = _direction * speed;
    }



    // Event
    protected void TriggerEvent() {
        onTreatment = true;
        StartCoroutine(waitForRandomEvent());
    }

    IEnumerator waitForRandomEvent () {    
        if(RandomProbaEvent()) {
            InitEvent();
        }

        yield return new WaitForSeconds(1f);

        onTreatment = false;
    }

    // Proba event
    protected bool RandomProbaEvent() {
        var rand = UnityEngine.Random.Range(0, proba + 1);  
        return (rand <= proba - 1) ? false : true;
    }


    protected void InitEvent () {

        // Arret drone + changement couleur => vert
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Renderer>().material.color = new Color32(0, 188, 50, 255);
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        status = Status.OnEvent;

        nbDronesActifs++;

        eventTimeInit = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        StartCoroutine(waitForGestionEvent());
    }


    IEnumerator waitForGestionEvent () {    
        yield return new WaitForSeconds(eventDuration / 2);   // Moitié => jaune
    
        if (status == Status.OnEvent) {
            GetComponent<Renderer>().material.color = new Color32(255, 196, 0, 255);
        } else {
            yield break;
        }

        yield return new WaitForSeconds(eventDuration / 4);   // 3/4 => rouge
    
        if (status == Status.OnEvent) {
            GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255);
        } else {
            yield break;
        }

        yield return new WaitForSeconds(eventDuration / 4);   // expiré
    
        if (status == Status.OnEvent) {
            Points.setPoints(Points.getPoints() - 1);
            GameObject.Find("Points").GetComponent<Text>().text = "Points " + Points.getPoints().ToString();

            Log.AddLog(2, nbDronesActifs, alert.getAlertStatus(), eventDuration, eventDuration);
            
            FinishEvent();
        }
    }

    protected virtual void FinishEvent() {
        SetInitialState();
        nbDronesActifs--;
    }

    public int getNbDronesActifs() {
        return nbDronesActifs;
    }
}
