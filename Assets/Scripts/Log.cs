using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Log : MonoBehaviour
{
    private string username;
    private string scenario;

    private string path = Directory.GetCurrentDirectory();

    private static string ficName;

    private static long timeInit;

    private static bool isLog = false;
    
    public static bool isServer = false;


    public void createNewFic(string s) {

        scenario = s;
        timeInit = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        ficName = "log_" + timeInit.ToString() + "_" + scenario + "_" + username + ".csv";

        InitializeEnv env = new InitializeEnv();

        File.WriteAllText(Directory.GetCurrentDirectory() + @"\Logs\" + ficName, "timestamp, temps en seconde, type, temps de réaction, temps total event, nbPoints, nbDronesActifs, alarmeActive\n");
    
        Points.setPoints(0);
        //AddLog(-1, 0, false);
        File.AppendAllText (Directory.GetCurrentDirectory() + @"\Logs\" + ficName, System.DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() + ", 0, -1, 0, 0, 0, 0, false\n");

        isLog = true;

    }


    public static void AddLog(int type, int nbDrones, bool alarme, long timeReaction, long timeEventTotal) {
        if (isLog) {
            long timeCurrent = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds() - timeInit;
         
            File.AppendAllText (Directory.GetCurrentDirectory() + @"\Logs\" + ficName, System.DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() + ", " + timeCurrent.ToString() + ", " + type + ", " + timeReaction + ", " + timeEventTotal + ", " + Points.getPoints() + ", " + nbDrones + ", " + alarme + "\n");
        }

        if(isServer) {
            if (type == 1 || type == 3) {
                
                GameObject.Find("SceneControl").GetComponent<TCPServer2>().sendMessageToClient("stopAlert");
            }    
        }
    }

    public void endFic() {
        File.AppendAllText (Directory.GetCurrentDirectory() + @"\Logs\" + ficName, "Fin simulation" + System.DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() + "\n");

        if(isServer) {
            GameObject.Find("SceneControl").GetComponent<TCPServer2>().sendMessageToClient("exit");  
            GameObject.Find("SceneControl").GetComponent<TCPServer2>().closeConnexion();  
        }

        setIsServer(false);
    }

    public void setUsername(string u) {  
        username = u;
    }

    public void setIsServer(bool s) {  
        TCPServer2.setIsServer(s);
        isServer = s;
    }
}
