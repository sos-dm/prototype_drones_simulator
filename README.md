# Prototype_drones_simulator


## Installation

```
git clone https://gitlab.imt-atlantique.fr/sos-dm/prototype_drones_simulator.git
```

Installer Unity Hub : https://unity.com/fr/download

Installer la version d'Unity 2020.3.23f1

Ouvrir le projet avec Unity Hub 


## Configuration

### Les drones

#### Nombres

Fenetre Project > Assets > Prefab > Table > Initialize Env > Total Number Of Drone

(By default 10)

#### Temps d'actions

Fenetre Project > Assets > Prefab > Drone > Cylinder > Drone AI Sc 1_V1 > Event Duration

(By default 15s)

#### Proba d'action

Fenetre Project > Assets > Prefab > Drone > Cylinder > Drone AI Sc 1_V1 > Proba

(By default 1 chance / 20 per seconde)

### Les alertes

#### Nombres

Fenetre Project > Assets > Prefab > Table > Initialize Env > Total Number Of Alarme

(By default 4)

#### Temps d'actions

Fenetre Project > Assets > Prefab > Alert > Alert V1 > length

(By default 5s)

#### Proba d'action

Fenetre Project > Assets > Prefab > Alert > Alert V1 > Proba

(By default 1 chance / 20 per seconde)
